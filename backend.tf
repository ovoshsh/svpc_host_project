terraform {
  backend "gcs" {
    bucket      = "ovosh-tfstate"
    credentials = "terraform-admin.json"
    prefix      = "svpc-dev"
  }
}