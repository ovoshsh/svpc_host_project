output "admin_sql_pwd" {
  value     = module.cloud_sql.db_password
  sensitive = true
}

output "db_ip" {
  value = module.cloud_sql.private_db_ip
}
