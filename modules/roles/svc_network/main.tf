resource "google_compute_network" "vpc_network" {
  project                 = var.project
  name                    = format("%s-%s", var.project, "vpc")
  auto_create_subnetworks = false
}