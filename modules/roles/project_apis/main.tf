# Based on https://github.com/terraform-google-modules/terraform-google-project-factory/tree/v2.4.1/modules/project_services
resource "google_project_service" "project_services" {
  for_each                   = toset(var.activate_apis)
  project                    = var.project_id
  service                    = each.key
  disable_dependent_services = true
}