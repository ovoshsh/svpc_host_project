
resource "google_compute_shared_vpc_host_project" "host" {
  project = var.project
}

resource "google_compute_shared_vpc_service_project" "app" {
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = var.app_project
}

resource "google_compute_shared_vpc_service_project" "db" {
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = var.db_project
}