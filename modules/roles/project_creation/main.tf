
resource "random_id" "id" {
  byte_length = 4
  prefix      = format("%s-", var.project_name)
}

resource "google_project" "project" {
  name                = var.project_name
  project_id          = random_id.id.hex
  folder_id           = var.gvars.folder_id
  billing_account     = var.gvars.billing_account
  auto_create_network = var.create_network
}

