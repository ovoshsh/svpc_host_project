resource "google_compute_region_instance_group_manager" "wordpress" {
  project  = var.project
  region   = var.gvars.region
  provider = google-beta

  name = format("%s-%s", var.gvars.roles.app_project.project_name, "ig")

  version {
    instance_template = google_compute_instance_template.wordpress.id
  }

  base_instance_name = format("%s-%s", var.gvars.roles.app_project.instance_name, "ig")
  target_size        = 1

  named_port {
    name = lower(var.gvars.roles.app_project.protocol)
    port = 80
  }

  lifecycle {
    create_before_destroy = true
  }
}

