data "google_compute_image" "image" {
  project = var.gvars.roles.app_project.source_image_project
  name    = var.gvars.roles.app_project.machine_image

}

resource "google_service_account" "app_sa" {
  project      = var.project
  account_id   = format("%s-app", var.gvars.roles.app_project.service_account.sa_name)
  display_name = "Project VM Service Account"
}

resource "google_project_iam_member" "project_roles" {
  project  = var.project
  for_each = toset(var.gvars.roles.app_project.service_account["roles"])
  role     = each.key
  member   = "serviceAccount:${google_service_account.app_sa.email}"
}

module "gce-container" {
  source = "github.com/terraform-google-modules/terraform-google-container-vm"

  container = {
    image = var.gvars.roles.app_project.container_image
    env = [
      {
        name      = "WORDPRESS_DB_NAME"
        value     = var.gvars.roles.app_project.wordpress_db_name
        sensitive = true
      },
      {
        name      = "WORDPRESS_DB_HOST"
        value     = format("%s:%s", var.db_host, "3306")
        sensitive = true
      },
      {
        name      = "WORDPRESS_DB_USER"
        value     = var.gvars.roles.app_project.wordpress_db_user
        sensitive = true
      },
      {
        name      = "WORDPRESS_DB_PASSWORD"
        value     = var.db_pwd
        sensitive = true
      }
    ]
  }

  restart_policy = "Always"
}

resource "google_compute_instance_template" "wordpress" {
  project      = var.project
  name         = format("%s-%s", var.gvars.roles.app_project.project_name, "01")
  machine_type = var.gvars.roles.app_project.machine_type

  disk {
    source_image = data.google_compute_image.image.self_link
    auto_delete  = true
    boot         = true
  }

  network_interface {
    subnetwork = var.subnet_link
    /*access_config {
      #nat_ip = ""
    }
    */
  }

  service_account {
    email  = google_service_account.app_sa.email
    scopes = var.gvars.roles.app_project.service_account.scopes
  }

  metadata = {
    google-logging-enabled    = true
    google-monitoring-enabled = true
    gce-container-declaration = module.gce-container.metadata_value
  }

  lifecycle {
    create_before_destroy = true
  }

}


