resource "google_compute_address" "external_address" {
  project = var.project
  region  = var.gvars.region
  name    = var.gvars.roles.svpc_project.cloud_nat.ip_name_static
}

resource "google_compute_router" "cloud-nat-router" {
  project = var.project
  name    = var.gvars.roles.svpc_project.cloud_nat.router_name
  region  = var.gvars.region
  network = var.network
}

resource "google_compute_router_nat" "cloud-nat" {
  project                            = var.project
  name                               = var.gvars.roles.svpc_project.cloud_nat.nat_name
  router                             = google_compute_router.cloud-nat-router.name
  region                             = google_compute_router.cloud-nat-router.region
  nat_ip_allocate_option             = var.gvars.roles.svpc_project.cloud_nat.nat_ip_allocate_option
  nat_ips                            = google_compute_address.external_address.*.self_link
  source_subnetwork_ip_ranges_to_nat = var.gvars.roles.svpc_project.cloud_nat.source_subnetwork_ip_ranges_to_nat

  /*
  # Allow all network
  subnetwork {
    name                    = var.network_name
    source_ip_ranges_to_nat = [var.gvars.roles.svpc_project.cloud_nat.source_ip_ranges_to_nat]
  }
  */
  subnetwork {
    name                    = var.subnetwork.id
    source_ip_ranges_to_nat = [var.gvars.roles.svpc_project.cloud_nat.source_ip_ranges_to_nat]
  }
  /*
  log_config {
    enable = true
    filter = var.gvars.roles.svpc_project.cloud_nat.log_config_filter
  }
  */
}
