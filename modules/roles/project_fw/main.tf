resource "google_compute_firewall" "subnet_access" {
  name    = "allow-icmp-sql"
  project = var.project
  network = var.network

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = var.gvars.roles.svpc_project.fw-access-from.ports
  }
}