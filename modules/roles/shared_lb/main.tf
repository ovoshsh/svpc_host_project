resource "google_compute_global_address" "ingress_ip" {
  project    = var.project
  name       = format("%s", "shared-ingress-ip")
  ip_version = "IPV4"
}

resource "google_compute_global_forwarding_rule" "default" {
  project = var.project
  name    = "wordpress-forwarding-rule"
  target  = google_compute_target_http_proxy.default.id

  ip_address = google_compute_global_address.ingress_ip.id
  port_range = var.gvars.roles.app_project.health-check.port
}

resource "google_compute_target_http_proxy" "default" {
  name    = "default-proxy"
  project = var.project
  url_map = google_compute_url_map.default.self_link
}

resource "google_compute_url_map" "default" {
  name            = "wordpress-map"
  project         = var.project
  default_service = google_compute_backend_service.backend.self_link
}


resource "google_compute_backend_service" "backend" {
  name    = "wordpress-backend"
  project = var.project

  protocol  = var.gvars.roles.app_project.protocol
  port_name = lower(var.gvars.roles.app_project.protocol)

  timeout_sec = 10

  enable_cdn            = false
  load_balancing_scheme = "EXTERNAL"

  backend {
    balancing_mode = "UTILIZATION"
    group          = var.instance
  }

  health_checks = [google_compute_health_check.default.id]
}

resource "google_compute_health_check" "default" {
  provider           = google-beta
  project            = var.project
  name               = "check-wordpress-backend"
  check_interval_sec = var.gvars.roles.app_project.health-check.check_interval
  timeout_sec        = var.gvars.roles.app_project.health-check.timeout

  http_health_check {
    port         = var.gvars.roles.app_project.health-check.port
    request_path = var.gvars.roles.app_project.health-check.url_path
  }
}
