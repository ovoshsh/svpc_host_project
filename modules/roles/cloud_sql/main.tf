resource "random_password" "pwd" {
  length  = var.gvars.roles.db_project.db_creds["pwd_length"]
  special = true
}
resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "sql_db_instance" {

  name             = format("%s-%s", var.gvars.roles.db_project.db_name, random_id.db_name_suffix.hex)
  database_version = var.gvars.roles.db_project.db_ver
  region           = var.gvars.region
  project          = var.project

  settings {
    tier = var.gvars.roles.db_project.machine_type_tier

    ip_configuration {

      ipv4_enabled    = false
      private_network = var.network
    }

    maintenance_window {
      day          = var.gvars.roles.db_project.maintenance_window.day
      hour         = var.gvars.roles.db_project.maintenance_window.hour
      update_track = var.gvars.roles.db_project.maintenance_window.update_track
    }

    #   ## Backup that DB instance
    backup_configuration {
      enabled            = true
      binary_log_enabled = true
      start_time         = "01:00"
    }


    dynamic "database_flags" {
      for_each = var.gvars.roles.db_project.database_flags
      iterator = flag
      content {
        name  = flag.key
        value = flag.value
      }
    }
  }
}

## CREATE SECRETMANAGER to save DB password <- Змінити

## ADD USERS
resource "google_sql_user" "admin_user" {
  name     = var.gvars.roles.db_project.db_creds.user
  instance = google_sql_database_instance.sql_db_instance.name
  host     = "%" ## Змінити
  password = random_password.pwd.result
  project  = var.project
}

# Create DB wordpress, maybe need to move in separate module
resource "google_sql_database" "database" {
  name     = var.gvars.roles.app_project.wordpress_db_name
  project  = var.project
  instance = google_sql_database_instance.sql_db_instance.name
}