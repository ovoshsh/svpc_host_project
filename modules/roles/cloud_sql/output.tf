
output "db_password" {
  value = random_password.pwd.result
}

output "db_instance" {
  value = google_sql_database_instance.sql_db_instance
}

output "private_db_ip" {
  value = google_sql_database_instance.sql_db_instance.private_ip_address
}