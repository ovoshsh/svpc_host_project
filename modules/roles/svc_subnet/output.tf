output "app" {
  value = google_compute_subnetwork.app
}

output "db" {
  value = google_compute_subnetwork.db
}