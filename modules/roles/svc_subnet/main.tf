resource "google_compute_subnetwork" "app" {
  provider      = google-beta
  project       = var.project
  name          = var.gvars.roles.svpc_project.app_net.subnet_name
  ip_cidr_range = var.gvars.roles.svpc_project.app_net.cidr
  region        = var.gvars.region
  network       = var.network
  purpose       = "PRIVATE"
  role          = "ACTIVE"
}

resource "google_compute_subnetwork" "db" {
  provider      = google-beta
  project       = var.project
  name          = var.gvars.roles.svpc_project.db_net.subnet_name
  ip_cidr_range = var.gvars.roles.svpc_project.db_net.cidr
  region        = var.gvars.region
  network       = var.network
  purpose       = "PRIVATE"
  role          = "ACTIVE"
}
