
resource "google_compute_global_address" "cloudsql-private-ip" {
  provider      = google-beta
  name          = "google-services-cloudsql-private-ip"
  project       = var.project
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 24
  network       = var.network
}

resource "google_service_networking_connection" "cloud-sql-connection" {
  provider                = google-beta
  network                 = var.network
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.cloudsql-private-ip.name]
}