locals {
  env_variables = {
    dev = jsondecode(file("./modules/vars/dev_svpc_host.json"))
  }
  project          = lookup(local.env_variables[terraform.workspace], "project")
  region           = lookup(local.env_variables[terraform.workspace], "region")
  zone             = lookup(local.env_variables[terraform.workspace], "zone")
  folder_id        = lookup(local.env_variables[terraform.workspace], "folder_id")
  credentials_file = lookup(local.env_variables[terraform.workspace], "credentials_file")
  bucket           = lookup(local.env_variables[terraform.workspace], "bucket")
  metadata         = lookup(local.env_variables[terraform.workspace], "metadata")
  labels           = lookup(local.env_variables[terraform.workspace], "labels")
  roles            = lookup(local.env_variables[terraform.workspace], "roles")
  billing_account  = lookup(local.env_variables[terraform.workspace], "billing_account")
}

output "project" {
  value = local.project
}

output "credentials_file" {
  value = local.credentials_file
}

output "region" {
  value = local.region
}

output "zone" {
  value = local.zone
}

output "folder_id" {
  value = local.folder_id
}

output "metadata" {
  value = local.metadata
}

output "labels" {
  value = local.labels
}

output "roles" {
  value = local.roles
}

output "bucket" {
  value = local.bucket
}

output "billing_account" {
  value = local.billing_account
}