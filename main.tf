module "gvars" {
  source = "./modules/vars/"
}

module "app_project" {
  for_each       = module.gvars.roles
  project_name   = try(each.value.project_name)
  create_network = try(each.value.create_network)
  source         = "./modules/roles/project_creation"
  gvars          = module.gvars
}

module "project_apis" {
  for_each   = module.gvars.roles
  source     = "./modules/roles/project_apis"
  project_id = module.app_project[try(each.key)].project_id
  activate_apis = [
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com",
    "servicenetworking.googleapis.com",
    "sqladmin.googleapis.com",
  ]
  depends_on = [module.app_project]
}


module "svc_network" {
  source     = "./modules/roles/svc_network"
  project    = module.app_project["svpc_project"].project_id
  gvars      = module.gvars
  depends_on = [module.project_apis]
}

module "svc_subnet" {
  source       = "./modules/roles/svc_subnet"
  project      = module.app_project["svpc_project"].project_id
  network      = module.svc_network.network_id
  network_name = module.svc_network.network_name
  gvars        = module.gvars
  depends_on   = [module.svc_network]
}

module "cloud_nat" {
  source       = "./modules/roles/cloud_nat"
  project      = module.app_project["svpc_project"].project_id
  network      = module.svc_network.network_id
  network_name = module.svc_network.network_name
  subnetwork   = module.svc_subnet.app
  gvars        = module.gvars
  depends_on   = [module.svc_network, module.project_apis]
}

module "shared_vpc" {
  source      = "./modules/roles/shared_vpc"
  project     = module.app_project["svpc_project"].project_id
  app_project = module.app_project["app_project"].project_id
  db_project  = module.app_project["db_project"].project_id
  gvars       = module.gvars
  depends_on = [module.project_apis]
}

module "cloud_sql" {
  source     = "./modules/roles/cloud_sql"
  network    = module.svc_network.network_id
  project    = module.app_project["db_project"].project_id
  gvars      = module.gvars
  depends_on = [module.shared_vpc]
}

module "app_instances" {
  # try to use COS
  source      = "./modules/roles/project_instance"
  project     = module.app_project["app_project"].project_id
  db_host     = module.cloud_sql.private_db_ip
  db_pwd      = module.cloud_sql.db_password
  subnet_link = module.svc_subnet.app.self_link
  gvars       = module.gvars
  depends_on  = [module.shared_vpc]
}

module "project_fw" {
  source  = "./modules/roles/project_fw"
  project = module.app_project["svpc_project"].project_id
  network = module.svc_network.network_self_link
  gvars   = module.gvars
}

module "shared_lb" {
  source   = "./modules/roles/shared_lb"
  project  = module.app_project["app_project"].project_id
  instance = module.app_instances.instance_group
  network  = module.svc_network.network_id
  gvars    = module.gvars
}
