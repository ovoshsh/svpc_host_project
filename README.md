# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* GCP: Shared VPC Network
* Managment with Terraform

### Task ###

1. Everything must be managed by Terraform with the state stored in the GCS bucket in a separate management project. Terraform must also manage API Enablements.

2. App and DB Service Projects must not have their own networks but rather utilize the specific subnets from SVPC Host Project.
	
3. Firewall rules need to be configured to only allow MySQL and ICMP traffic between the App and DB instances.
	
4. Web app hosted on App instance must be exposed via LoadBalancer to the public Internet.
	
5. Usage of official Google Terraform modules (https://registry.terraform.io/providers/hashicorp/google/latest) is required.
	
6. All of Terraform resource code must consist only of module calls, no direct resource creation.

### Solving ###
1. Databases instance cloud SQL
2. As a SQL based application it uses wordpress
3. Managment project and bucket should be already created
4. SA for terraform should have all necessary roles and permissions for access to resources. 
   * roles/billing.user - cate project and enable API
   * roles/compute.xpnAdmin - for Shared VPC
   


![img.png](img.png)
